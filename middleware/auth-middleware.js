const jwt = require('jsonwebtoken');
const config = require('config');
module.exports = async (req, res, next) => {
    if(req.method === 'OPTIONS') {
       return next();
    }

    try {
        // console.log('Req headers authorization: ',req.headers.authorization);
        const token = req.headers.authorization.split(' ')[1];
        // console.log('Token: ',token);
        if(!token) {
            return res.status(401).json({message: 'not authorized'});
        }
        const decoded = jwt.verify(token, config.get('JWT_SECRET'));
        // console.log('Decoded : ',decoded);
        req.user = decoded;
        next();
    }
    catch (e) {
        return res.status(401).json({message: 'not authorized'});
    }

}