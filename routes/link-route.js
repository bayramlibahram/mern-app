const {Router, json} = require('express');
const router = Router();
const Link = require('../models/link');
const config = require('config');
const shortId = require('shortid');
const authMiddleware = require('../middleware/auth-middleware');

router.get('/', authMiddleware, async (req, res) => {
    try {
        const links = await Link.find({owner: req.user.userId});
        res.json(links);
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

router.get('/:id', authMiddleware, async (req, res) => {
    try {
        const link = await Link.findById(req.params.id);
        res.json(link);
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

router.post('/generate', authMiddleware, async (req, res) => {
    try {
        const {from} = req.body;

        const linkExisting = await Link.findOne({from});

        if (linkExisting) {
            return res.json({message: 'link is already existing'});
        }

        const baseUrl = config.get('BASE_URL');
        const code = shortId.generate();
        const to = `${baseUrl}/t/${code}`;

        const link = new Link({
            from,
            to,
            code,
            owner: req.user.userId
        });

        await link.save();
        res.json(link);

    } catch (err) {
        res.status(500).json({message: err.message});
    }
});


module.exports = router;