const {Router} = require('express');
const AuthController = require('../controllers/auth-controller');
const {login, register} = new AuthController();
const {registerValidator, loginValidator} = require('../utils/validators');
const router = Router();

router.post('/login', loginValidator, login);
router.post('/register', registerValidator, register);

module.exports = router;