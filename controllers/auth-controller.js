const User = require('../models/user');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('config');

class AuthController {
    login = async (req, res) => {
        try {
            const errors = validationResult(req);

            if(!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Incorrect fields value on login'
                });
            }

            const {email, password} = req.body;

            const user = await User.findOne({ email });

            if(!user) {
                return res.status(401).json({message: 'User not found'});
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if(!isMatch) {
                return res.status(401).json({message: 'Email or password is incorrect.'});
            }

            const token = jwt.sign(
                {userId:user.id},
                config.get('JWT_SECRET'),
                {expiresIn: config.get('EXPIRES_DATE')}
            );

            res.json({token, userId:user.id});
            
        }
        catch (e) {
            res.status(500).json({requestApi: '/api/register', errorMessage: e.message});
        }
    }
    register = async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Incorrect credentials in registration'
                });
            }

            const {email, password} = req.body;

            const candidate = await User.findOne({email});

            if (candidate) {
                return res.status(400).json({message: 'User is already exists'});
            }

            const hashPassword = await bcrypt.hash(password, 10);

            const user = new User({email, password: hashPassword});

            await user.save();

            res.status(201).json({message: `${user.email} is created`});


        }
        catch (e) {
            res.status(500).json({requestApi: '/api/login', errorMessage: e.message});
        }
    }
}

module.exports = AuthController;