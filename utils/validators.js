const {check} = require('express-validator');

exports.registerValidator = [
    check('email', 'Email is incorrect').isEmail(),
    check('password', 'Minimal length must be at least 6 symbols')
        .isLength({min: 6}),
];

exports.loginValidator = [
    check('email', 'Type correct email')
        .normalizeEmail()
        .isEmail(),
    check('password', 'Password field is empty')
        .exists()
        .isLength({min: 6})
];