import React, {useContext, useEffect, useState} from 'react';
import './auth-page.css';
import {useHttp, useMessage} from "../../hooks";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {AuthContext} from "../../context";

const AuthPage = () => {

    const auth = useContext(AuthContext);

    const message = useMessage();

    const {loading, request, error, clearError} = useHttp();

    const [form, setForm] = useState({email: '', password: ''});

    useEffect(() => {
        message(error);
        clearError();
    }, [error, message, clearError]);

    useEffect(() => {
        window.M.updateTextFields();
    },[]);

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value});
    }

    const registerHandler = async () => {
        try {
            const data = await request(
                '/api/auth/register',
                'POST',
                {...form}
            );
            message(data.message);
        } catch (e) {
        }
    }

    const loginHandler = async () => {
        try {
            const data = await request(
                '/api/auth/login',
                'POST',
                {...form}
            );
            console.log(data);
            auth.login(data.token, data.userId);
        } catch (e) {
        }
    }

    return (
        <div className="row">
            <div className="col s6 offset-s3">
                <h1>Short the link</h1>
                <div className="card">
                    <div className="card-content">
                        <span className="card-title">Authorization</span>
                        <div>
                            <div className="input-field">
                                <input
                                    placeholder="type email address"
                                    id="email"
                                    name="email"
                                    type="text"
                                    className="validate"
                                    onChange={changeHandler}
                                />
                                <label htmlFor="email">Email</label>
                            </div>
                            <div className="input-field">
                                <input
                                    placeholder="type password"
                                    id="password"
                                    name="password"
                                    type="password"
                                    className="validate"
                                    onChange={changeHandler}
                                />
                                <label htmlFor="password">Password</label>
                            </div>
                        </div>
                    </div>
                    <div className="card-action">
                        <button
                            className="btn yellow darken-4 login-button"
                            onClick={loginHandler}
                            disabled={loading}>
                            Login
                        </button>
                        <button
                            className="btn lighten-1 black-text"
                            onClick={registerHandler}
                            disabled={loading}>
                            Register
                        </button>
                    </div>
                </div>
            </div>
            <ToastContainer/>
        </div>
    );
}

export default AuthPage;