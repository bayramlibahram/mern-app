import React, {useCallback, useContext, useEffect, useState} from 'react';
import {useHttp} from "../../hooks";
import {AuthContext} from "../../context";
import Spinner from "../../components/spinner";
import LinksList from "../../components/links-list";

const LinksPage = () => {
    const [links, setLinks] = useState();
    const {request, loading} = useHttp();
    const {token} = useContext(AuthContext);

    const fetchedLinks = useCallback(async () => {
        const fetched = await request('/api/link', 'GET', null, {
            Authorization: `Bearer ${token}`
        });
        setLinks(fetched);
    },[ token, request]);

    useEffect(() => {
       fetchedLinks()
    },[fetchedLinks]);

    if(loading) {
        return <Spinner/>;
    }

    console.log('links page',links);

    return (
        <div>
            {!loading && <LinksList links={links}/>}
        </div>
    )
}

export default LinksPage;