import React, {useEffect, useCallback, useState, useContext} from 'react';
import {useParams} from 'react-router-dom';
import {useHttp} from "../../hooks";
import {AuthContext} from "../../context";
import Spinner from "../../components/spinner";
import LinkCard from "../../components/link-card";

const DetailPage = () => {
    const {token} = useContext(AuthContext);
    const [link, setLink] = useState(null);
    const linkId = useParams().id;
    const {request, loading} = useHttp();

    const getLink = useCallback(async () => {
        try {
            const fetched = await request(`/api/link/${linkId}`, 'GET', null, {
                Authorization: `Bearer ${token}`
            });
            setLink(fetched);
        } catch (e) {
        }
    }, [token, linkId, request]);

    useEffect(() => {
        getLink()
    }, [getLink]);

    if (loading) {
        return <Spinner/>
    }

    return (
        <div>
            {!loading && link && <LinkCard link={link}/>}
        </div>
    )
}

export default DetailPage;