import React, {useContext, useEffect, useState} from 'react';
import './create-page.css'
import {useHttp} from '../../hooks';
import {AuthContext} from "../../context";
import {useNavigate} from "react-router-dom";

const CreatePage = () => {

    const {request} = useHttp();
    const [link, setLink] = useState('');
    const auth = useContext(AuthContext);
    const navigate = useNavigate();

    const pressHandler = async event => {
        try {
            if (event.key === 'Enter') {
                const data = await request('/api/link/generate', 'POST', {from: link},
                    {Authorization: `Bearer ${auth.token}`}
                );
                navigate(`/detail/${data._id}`);
            }
        }
        catch (e) {

        }
    }

    useEffect(() => {
        window.M.updateTextFields();
    }, []);

    return (
        <div className="row">
            <div className="col s8 offset-s2 create-page">
                <div className="input-field">
                    <input
                        type="text"
                        placeholder="type the link url"
                        id="link"
                        value={link}
                        onChange={event => setLink(event.target.value)}
                        onKeyPress={pressHandler}
                    />
                    <label htmlFor="link">Link name</label>
                </div>
            </div>
        </div>
    )
}

export default CreatePage;