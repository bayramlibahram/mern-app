import AuthPage from "./auth-page";
import LinksPage from './links-page';
import CreatePage from "./create-page";
import DetailPage from "./detail-page";


export {
    AuthPage,
    LinksPage,
    CreatePage,
    DetailPage
};