import {useCallback} from "react";
import {  toast } from 'react-toastify';

const useMessage = () => {
    return useCallback(messageText => {
        if(messageText) {
            toast(messageText);
        }
    }, []);
}


export default useMessage;