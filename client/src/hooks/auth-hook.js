import {useState, useCallback, useEffect} from 'react';

const storageName = 'userData';

const useAuth = () => {
    const [token, setToken] = useState(null);
    const [userId, setUserId] = useState(null);
    const [ready, setReady] = useState(false);

    const login = useCallback((jwToken, id) => {
        setToken(jwToken);
        setUserId(id);
        localStorage.setItem(storageName, JSON.stringify({
            userId: id,
            token: jwToken
        }));
    }, []);

    const logout = useCallback(() => {
        setUserId(null);
        setToken(null);
        localStorage.removeItem(storageName);
    }, []);


    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName));
        if (data && data.token) {
            login(data.token, data.userId);
        }
        setReady(true);
    }, [login, ready]);


    return {
        login,
        logout,
        token,
        userId,
        ready
    }
}

export default useAuth;