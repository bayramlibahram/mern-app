import React, {useContext} from "react";
import {Link, useNavigate} from "react-router-dom";
import {AuthContext} from "../../context";

const Navbar = () => {
    const auth = useContext(AuthContext);
    const navigate = useNavigate();
    const logoutHandler = event => {
        event.preventDefault();
        auth.logout();
        navigate("/", { replace: true });
    }
    return(
        <nav>
            <div className="nav-wrapper blue darken-1">
               <div className="container">
                   <Link to="/" className="brand-logo">Short link</Link>
                   <ul id="nav-mobile" className="right hide-on-med-and-down">
                       <li><Link to="/create">Create</Link></li>
                       <li><Link to="/links">Links</Link></li>
                       <li><Link to="/" onClick={logoutHandler}>Log out</Link></li>
                   </ul>
               </div>
            </div>
        </nav>
    )
}

export default Navbar