import React from "react";
import {BrowserRouter as Router} from "react-router-dom";
import useRoutes from "../../routes";
import {useAuth} from "../../hooks";
import {AuthContext} from "../../context";
import 'materialize-css';
import Navbar from "../navbar/navbar";
import Spinner from "../spinner";

const App = () => {
    const {login, logout, userId, token, ready} = useAuth();
    const isAuthenticated = !!token;
    const routes = useRoutes(isAuthenticated);

    if(!ready) {
        return <Spinner />
    }

    const providerValue = {login,logout, token,userId};
    return (
        <AuthContext.Provider value={providerValue}>
            <Router>
                {isAuthenticated && <Navbar/>}
                <div className="container">
                    {routes}
                </div>
            </Router>
        </AuthContext.Provider>
    )
}

export default App;