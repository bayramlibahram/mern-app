import React from 'react';
import {Link} from "react-router-dom";


const LinksList = ({links}) => {

    if(!links || !links.length) {
        return (
            <p>
                There is no links
            </p>
        )
    }

    return (
        <div>
            <table>
                <thead>
                <tr>
                    <th>N</th>
                    <th>Original Link</th>
                    <th>Short link</th>
                    <th> Action</th>
                </tr>
                </thead>

                <tbody>
                {
                    links.map((link, index) => {
                        console.log('Map', link)
                            return (
                                <tr key={link._id}>
                                    <td>{index + 1}</td>
                                    <td>{link.from}</td>
                                    <td>{link.to}</td>
                                    <td>
                                        <Link to={`/detail/${link._id}`}>Open link</Link>
                                    </td>
                                </tr>
                            )
                    })
                }
                </tbody>
            </table>

        </div>
    )
}


export default LinksList