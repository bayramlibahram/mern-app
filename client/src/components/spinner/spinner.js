import React from 'react';
import './spinner.css';
const Spinner = () => {
    return (
        <div className="loadingio-spinner-pulse-4vyrvrrnaim">
            <div className="ldio-0m0f2l8ud8ui">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    )
}

export default Spinner;