import React from 'react';
import {Routes, Route, Navigate} from 'react-router-dom';
import {LinksPage, CreatePage, DetailPage, AuthPage} from "../pages";

const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Routes>
                <Route path="/create" exact element={<CreatePage/>}/>
                <Route path="/links" exact  element={<LinksPage/>}/>
                <Route path="/detail/:id"  element={<DetailPage/>}/>
            </Routes>
        );
    }

    return (
        <Routes>
            <Route path="/"  element={<AuthPage/>}/>
            <Route
                path="*"
                element={<Navigate to="/"/>}
            />
        </Routes>
    );
}

export default useRoutes;