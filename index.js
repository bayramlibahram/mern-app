const express       = require('express');
const app           = express();
const config        = require('config');
const mongoose      = require('mongoose');
const path          = require('path');
const authRoute     = require('./routes/auth-route');
const linkRoute     = require('./routes/link-route');
const redirectRoute = require('./routes/redirect-route');

app.use(express.json({extended: true}));

app.use('/api/auth', authRoute);
app.use('/api/link', linkRoute);
app.use('/t/', redirectRoute);


if(process.env.NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname,'client', 'build')));

    app.get('*', async  (req,res) => {
        res.sendFile(path.resolve(__dirname,'client','build', 'index.html'));
    });
}

const start = async (PORT, MONGO_URL) => {
    await mongoose.connect(MONGO_URL)
        .then(() => {
            console.log('DB is connected successfully');
        })
        .catch(err => {
            console.log(err);
        });

    app.listen(PORT, () => {
        console.log(`Server has been listening on port ${PORT}`);
    });
}

const PORT = config.get('PORT');
const MONGO_URL = config.get('MONGO_URL');

start(PORT, MONGO_URL);
